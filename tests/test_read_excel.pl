#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: test_read_excel.pl
#
#        USAGE: ./test_read_excel.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 09.04.2016 19:15:43
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
use FindBin qw($Bin);
use File::Basename;
use Test::More qw 'no_plan';
use JSON::XS;
use Encode;


my $EXCEL_DIR = "$Bin/excel";
my $SCRIPT = "$Bin/../read_excel.pl";



my $TEST = {
				'ostatki-07.04.16.xls' => 
											[
												['Sheet1', 20, 3, 'Автошина  Sava  175/70  R13  82  T  Eskimo S 3+'],
												['Sheet1', 39, 5, 'X-Ice North 3'],
												['Sheet1', 148, 9, 'Зима'],
											],
				'07.04.16 московские остатки СЕВЕР-АВТО НН.xls' => 
											[
												['Легковая резина', 30, 2, 'Snow Grip  SATOYA  185/70/14  T  Ш.'],
												['Диски', 83, 8, 'Штамп.'],
												['Диски', 121, 14, '@кол.отверс.'],
												['Остальной товар', 106, 2, 'HWL  HT 315 гайка  19/21 12х1,25 L34 конус'],
											],
				'07.04.16 Наличие Москва.xls' => 
											[
												['Легковая резина', 88, 2, 'Амтел НордМастер ST  222В  Амт  205/70/15  Q  Ш.'],
												['Диски',  127, 6, '720'],
												['Грузовая резина', 39, 2, 'SOLIDEAL 16.9-28 SL R4 SD TL PR12 Индустриальная Пневматическая'],
											],
				'07.04.16 Наличие Уфа.xls' => 
											[
												['Легковая резина', 1327, 11, 'HAKKA BLUE XL'],
												['Диски',  187, 13, '7,0'],
												['Грузовая резина', 70, 11, '285/70R19.5'],
												['Остальной товар', 87, 8, 'Диски прочее'],
											],
				'M2126.xls' => 
											[
												['Шины (Склад Москва)', 145, 1, 'PXR0978603'],
												['Шины (Склад 2)',  145, 1, 'T429023'],
												['Шины (Склад 3)', 145, 8, 'Летняя'],
												['Диски (Склад Москва)', 40, 1, 'WHS087613'],
											],
		};


foreach my $xls_fname (glob "$EXCEL_DIR/*.xls") {
	$xls_fname = decode('utf-8', $xls_fname);
	my $basename = basename ($xls_fname);
	if (my ($key) = grep { $_ eq $basename } keys %$TEST) {
		$xls_fname =~ s/ /\\ /g;
		my $res = decode_json (encode ('utf-8', `$SCRIPT --limit=0 $xls_fname`));
		foreach my $test_record (@{$TEST->{$key}}) {
			my $sheet = $test_record->[0];
			my $row = $test_record->[1] - 1;
			my $cell = $test_record->[2] - 1;
			my $value = $test_record->[3];
			is ($res->{$sheet}->[$row]->[$cell], $value, "File: $basename. Sheet: $sheet Cell: $row:$cell" );
		}
	}
}

