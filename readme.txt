sudo apt-get install libxml2-dev
sudo cpan XML::LibXML::Reader
sudo cpan JSON::XS
sudo cpan Archive::Zip
sudo cpan OLE::Storage_Lite
sudo cpan Spreadsheet::ParseExcel

Use script:
	perl read_excel_tyres.pl --limit=<30> <xls filename>
	--limit - The number of rows to print, 0 - unlimit
	<xsl filename> - path to xls or xlsx file
