#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: read_excel_tyres.pl
#
#        USAGE: ./read_excel_tyres.pl  
#
#  DESCRIPTION: 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YOUR NAME (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 05.04.2016 19:51:01
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use open qw(:std :utf8);
use FindBin qw($Bin);
use lib "$Bin/excel-reader-xlsx/lib";
use File::Basename;
use Carp qw /croak/;
use JSON::XS;
use Encode;
use Excel::Reader::XLSX v0.01;
use Spreadsheet::ParseExcel;
binmode STDOUT;


main ();

sub main {
	my ($limit, $xls_fname) = parse_param();

	my $data;
	if ( $xls_fname =~ /\.xlsx$/ ) {
		$data = read_xlsx( -xls_fname => $xls_fname, -limit => $limit );
	}
	elsif ( $xls_fname =~ /\.xls$/ ) {
		$data = read_xls( -xls_fname => $xls_fname, -limit => $limit );
	}
	else {
		croak "Wrong file format $xls_fname";
	}
	print JSON::XS->new->utf8->encode( $data );

}



sub read_xls {
	my %opt = (
				-xls_fname 	=> '',
				-limit		=> 0,
				@_);
	my $data = {};

	my $parser = Spreadsheet::ParseExcel->new();
	my $workbook = $parser->parse( $opt{-xls_fname} ) or croak "Cant read $opt{-xls_fname}";

	for my $worksheet ( $workbook->worksheets() ) {
		my $limit = $opt{-limit};
		my ( $row_min, $row_max ) = $worksheet->row_range();
		my ( $col_min, $col_max ) = $worksheet->col_range();

	   	for (my $row = $row_min, $limit = $opt{-limit}; $row <= $row_max; $row++ ) {
			my @values;
			for my $col ( $col_min .. $col_max ) {
				my $cell = $worksheet->get_cell( $row, $col );
				if ($cell) {
					#Change encoding
					if ($cell->encoding () == 3) {
						$cell = decode('cp-1251', $cell->value());
					}
					else {
						$cell = $cell->value();
					}
				}
				else {
					$cell = '';
				}
				push @values, $cell;
			}
			push @{$data->{ $worksheet->get_name() }}, \@values;
			last if --$limit == 0;
		}
	}

	return $data;
}

sub read_xlsx {
	my %opt = (
				-xls_fname 	=> '',
				-limit		=> 0,
				@_);
	my $data = {};
	my $reader = Excel::Reader::XLSX->new() or croak "Cant create obj Excel::Reader::XLSX";
	my $workbook = $reader->read_file( $opt{-xls_fname} ) or croak "Cant read $opt{-xls_fname}";


	for my $worksheet ( $workbook->worksheets() ) {
		my $sheetname = $worksheet->name();
		my $limit = $opt{-limit};

		while ( my $row = $worksheet->next_row() ) {
			my @values = $row->values();
			push @{$data->{$sheetname}}, \@values;
			last if --$limit == 0;
		}
	}

	return $data;

}


sub parse_param {
	#Parsing params in ARGS
	my ($limit, $xls_fname);
	usage() if @ARGV != 2;
	if ($ARGV[0] =~ /^--limit=(\d+)$/) {
		$limit = $1;
	}
	else {
		usage();
	}
	$xls_fname = $ARGV[1];
	if (!-e $xls_fname) {
		croak "Filename $xls_fname not exists";
	}
	return ($limit, $xls_fname);
}


sub usage {
	print STDERR << "EOF";
use: perl $0 --limit=<30> <xls filename>
EOF
	exit;
}
